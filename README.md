# Coding Test - Open Restaurants
## Instructions to Boot and Test

- Requires NodeJs. This app is tested on Node 16

Install dependencies:
- npm install

Run app:
- Get a terminal to the app folder
- To run the app enter: node index
- To change search parameters, edit the index.js file on line 18.

Run tests:
- Get a terminal to the test folder
- To run tests enter: mocha restaurants_test

## Technical Overview
Architectural Summary:
- the app is a single file with many functions.
- functions include a parser to create a list of searchable json objects.
- the parser splits and formats the inputs with these functions: getTimeParts(), formatOpeningHours()
- there is some logic to identify subtleties and edge cases (week rollover, and night rollovers to the following day ) These are encapsulated in the functions: getOpenDays(), splitPmRollOvers().
- there is also function for search. This requires time comparison logic found in the function getRestaurantsOpenAt()

Technical Justification:
- A functional approach was used. There are lots of simple functions. These feed into larger functions. This allows for good test coverage. It is also easy to implement.
- TDD was also used. It proved very useful for identifying bugs and preventing regression.
- key design decisions: I did not use the Luxon library for calculating time. It was simpler to create my own time objects and comparer functions.
- other possible approaches: In hindsight: I should have used integer ranges instead of custom time objects. That approach would have been a little simpler. However, I am still satisfied with the current implementation.

Implementation Notes:
- 2 more records were added to the data file. This is to cover edge cases not in the original data.
- the first record contains times for a week rollover. Takanini Coffee Club is opened from Sat-Tue.
- the other record is for multiple day night rollover to the next day. Melba Manukau opens Mon-Tue 9:30 pm - 2 am.
- edge cases above are covered by unit tests.


Possible Improvements and Requests for Feedback
- Typing would definitely be of use, especially for the time objects. This could be a bit confusing for a new developer. However, I have more experience in plain untyped js. For now I am most proficient with this approach.
- I believe that the Luxon library is unnecessary. The app in its current form does not require the use of months/years. I chose to leave the Luxon implementations, since they were provided in the starter files.


