const expect = require("chai").expect;
const luxon = require("luxon");

const Restaurants = require("../app/restaurants.js");

const restaurantDataFile = "../restaurant_data.json";

describe("Restaurants class", function () {
  let restaurants;

  beforeEach(() => {
    restaurants = new Restaurants(restaurantDataFile);
  });

  // Helper function that returns the restaurants open on a specific weekday
  // at a given time. Monday is weekday === 0, and Sunday is weekday === 6.
  const getRestaurantsOpenAt = ({ weekday, hour, minute = 0 }) => {
    return restaurants.getRestaurantsOpenAt(
      luxon.DateTime.local(2021, 5, 10 + weekday, hour, minute)
    );
  };

    it("reports no open restaurants at 5am on Sundays", () => {
        expect(getRestaurantsOpenAt({ weekday: 6, hour: 5 })).to.deep.equal([]);
    });

    it("returns sequence days of the week", () => {
        expect(restaurants.getOpenDays('Wed', 'Fri')).to.deep.equal(['Wed', 'Thu', 'Fri']);
    });

    it("returns rolled over days of the week", () => {
        expect(restaurants.getOpenDays('Sat', 'Tue')).to.deep.equal(['Sat', 'Sun', 'Mon', 'Tue']);
    });

    it("returns time part with am hour", () => {
        expect(restaurants.getTimeParts('1 am')).to.deep.equal({hour: 1});
    });

    it("returns time part with pm hour", () => {
        expect(restaurants.getTimeParts('12 pm')).to.deep.equal({hour: 12});
    });

    it("returns time parts with hour and minute", () => {
        expect(restaurants.getTimeParts('10:30 pm')).to.deep.equal({hour: 22, minute: 30});
    });

    it("returns opening time object with single day string", () => {
        expect(restaurants.formatOpeningHours('Tue 11:50 am - 1 am'))
            .to.deep.equal({ days: ['Tue'], openingTime: { hour: 11, minute: 50 }, closingTime: { hour: 1 } });
    });

    it("returns opening time object with multiple days string", () => {
        expect(restaurants.formatOpeningHours('Tue-Fri 11:50 am - 1 am'))
            .to.deep.equal({ days: ['Tue', 'Wed', 'Thu', 'Fri'], openingTime: { hour: 11, minute: 50 }, closingTime: { hour: 1 } });
    });

    it("returns correct minutes when single day pm is passed in", () => {
        expect(restaurants.getOpenMinutes({ days: ['Tue'], openingTime: { hour: 13, minute: 50 }, closingTime: { hour: 15 } }))
            .to.deep.equal([[ 2270, 2340 ]]);
    });

    it("returns correct minutes when multiple day am is passed in", () => {
        expect(restaurants.getOpenMinutes({ days: ['Tue', 'Wed', 'Thu'], openingTime: { hour: 11, minute: 50 }, closingTime: { hour: 15 } }))
            .to.deep.equal([[2150, 2340], [3590, 3780], [5030, 5220]]);
    });


    it("returns correct minutes when roll over to next week", () => {
        expect(restaurants.getOpenMinutes({ days: ['Sun', 'Mon'], openingTime: { hour: 13, minute: 50 }, closingTime: { hour: 1 } }))
            .to.deep.equal([[ 9470, 10079 ], [0, 60], [830, 1500]]);
    });

    it("reports only the Kayasa Restaurant open on Monday at 8:30 am", () => {
        expect(
            getRestaurantsOpenAt({ weekday: 0, hour: 8, minute: 30 })
        ).to.deep.equal(["Kayasa Restaurant"]);
    });

    it("reports the Kayasa Restaurant and Takanini Coffee Club open on Monday at 10 am", () => {
        expect(
            getRestaurantsOpenAt({ weekday: 0, hour: 10, minute: 0 })
        ).to.deep.equal(["Kayasa Restaurant", "Takanini Coffee Club"]);
    });

    it("reports only the Melba Manukau Restaurant open on Wednesday at 1.59 am", () => {
        expect(
            getRestaurantsOpenAt({ weekday: 2, hour: 1, minute: 59 })
        ).to.deep.equal(["Melba Manukau"]);
    });

    it("reports no open restaurants at Wednesday at 2 am", () => {
        expect(
            getRestaurantsOpenAt({ weekday: 2, hour: 2, minute: 0 })
        ).to.deep.equal([]);
    });
});
