const luxon = require("luxon");

const Restaurants = require("../app/restaurants.js");

const restaurantDataFile = "../restaurant_data.json";

const restaurants = new Restaurants(restaurantDataFile);

// Helper function that returns the restaurants open on a specific weekday
// at a given time. Monday is weekday === 0, and Sunday is weekday === 6.
const getRestaurantsOpenAt = ({ weekday, hour, minute = 0 }) => {
    return restaurants.getRestaurantsOpenAt(
        luxon.DateTime.local(2021, 5, 10 + weekday, hour, minute)
    );
};


// Edit search parameters here:
const search = {weekday: 2, hour: 1, minute: 59};

console.log('\n ***** Welcome to the Restaurant Opening Hours App ***** \n')
console.log(`The search parameters are:\n`)
console.log(`Day: ${restaurants.days[search.weekday]}`)
console.log(`Hour: ${search.hour}`)
console.log(`Minute: ${search.weekday}\n`)
console.log('Search results are...>>>')
console.log( getRestaurantsOpenAt(search) );
