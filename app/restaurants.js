const fs = require("fs");
const luxon = require("luxon");

const DAYS = [ 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat', 'Sun'];

const getWeekMinute = (day, time) => DAYS.indexOf(day) * 24 * 60 + time.hour * 60 + (time.minute ? time.minute : 0);

/**
 * This class takes the name of a JSON file containing details on opening hours
 * for a number of restaurants. It parses the contents of this file and then
 * provides a method for querying which restaurants are open at a specified date
 * and time. The input JSON file can be assumed to contain correctly formatted
 * data.
 *
 * All dates and times can be assumed to be in the same time zone.
 */
class Restaurants {
    parsedData;
    days;
    constructor(jsonFilename) {
        const jsonData = JSON.parse(fs.readFileSync(jsonFilename));
        this.parsedData = this.parseRestaurants(jsonData);
        this.days = DAYS;
    }

    /**
     * Finds the restaurants open at the specified time.
     *
     * @param {luxon.DateTime} time
     * @returns {Array<string>} The names of the restaurants open at the specified
     * time. The order of the elements in this array is alphabetical.
     */
    getRestaurantsOpenAt(time) {

        const { weekday, hour, minute } = time;

        // luxon lib weekdays enumerated 1 to 7
        const day = DAYS[weekday - 1];

        const isOpen = (openingHours) => openingHours.findIndex(openHours => {
            return openHours[0] <= getWeekMinute(day, {hour, minute})
                && getWeekMinute(day, {hour, minute}) < openHours[1]
        }) > -1;

        return this.parsedData
        .filter(restaurant => isOpen(restaurant.openingHours))
        .map(restaurant => restaurant.name);
    }

    /**
     * Finds the days when a restaurant is open.
     *
     * @param {string, string} dayFrom, dayTo
     * @returns {Array<string>} The days of the week the restaurant is open.
     */
    getOpenDays(dayFrom, dayTo) {
        const getInd = (day) => DAYS.indexOf(day);
        const from = getInd(dayFrom);
        const to = getInd(dayTo);
        if (from < to) return DAYS.slice(from, to + 1);

        // else week days roll over
        return [...DAYS.slice(from, DAYS.length), ...DAYS.slice(0, to + 1)]
    }

    /**
     * Gets the time parts from a string
     *
     * @param {string} timeStr
     * @returns {Object} The luxon time parameter.
     */
    getTimeParts(timeStr) {

        const isAfterNoon = timeStr.indexOf('pm') > -1;

        // '9 pm' or '9:30 pm'  becomes '9 pm' or '9' '30 pm'
        const strParts = timeStr.split(':');
        let tempHour = parseInt(strParts[0].substr(0, 2));
        // correct edge case: for 12 am , hour = 0
        if (tempHour === 12 && !isAfterNoon) tempHour = 0;
        const hour = isAfterNoon && tempHour < 12 ? tempHour + 12 : tempHour;
        if (!strParts[1]) return { hour };

        // else include minutes
        const minute = parseInt(strParts[1].split(' ')[0]);
        return { hour, minute }
    }

    /**
     * Formats opening hours substr and returns object
     *
     * @param {string} openingHours substr
     * @returns {Object} Object with days, openingTime, closingTime
     * span to next day
     */
    formatOpeningHours(openingHours) {
        const isMultiDays = (openingHours.split('-').length - 1) > 1;

        const days = isMultiDays
            ? this.getOpenDays(openingHours.substr(0,3), openingHours.substr(4,3))
            : [openingHours.substr(0,3)];

        const [ openingTime, closingTime ] = openingHours
            .slice(isMultiDays ? 8 : 4)
            .split(' - ')
            .map(openHours => this.getTimeParts(openHours));

        return { days, openingTime, closingTime };
    }

    /**
     * Calculates the weeks' open minutes from the formatted hours
     *
     * @param {Object} formattedHours with days, openingTime, closingTime
     * @returns {Array<Array<number>>} Array of tuple:[ ...[open, close] ]week minutes
     *
     */
    getOpenMinutes(formattedHours) {
        const { days, openingTime, closingTime } = formattedHours;

        const getNextDay = (day) => DAYS[(DAYS.indexOf(day) + 1) % DAYS.length];

        // return if no rollover
        if (openingTime.hour * 60 + (openingTime.minute ? openingTime.minute : 0)
            < closingTime.hour * 60 + (closingTime.minute ? closingTime.minute : 0))
            return days.map(d => [[ getWeekMinute(d, openingTime), getWeekMinute(d, closingTime) ]]).flat();

        return days.map(day  => {
            if (day !== 'Sun')
                return [[getWeekMinute(day, openingTime), getWeekMinute(getNextDay(day), closingTime)]];

            // roll over has occurred from Sunday - Monday
            const [ startOfWeek, endOfWeek ] = [ 0, 7 * 24 * 60 - 1 ];
            return [
                [ getWeekMinute(day, openingTime), endOfWeek ],
                [ startOfWeek, getWeekMinute(getNextDay(day), closingTime) ]
            ]
        }).flat();
    }

    /**
     * Parse json data and return a searchable list
     *
     * @param {Object} jsonData
     * @returns {Array<Object>}
     *
     */
    parseRestaurants = (jsonData) =>
    jsonData.restaurants
    .map(restaurant => {
        const openingHours = restaurant.opening_hours
            .split('; ')
            .map(hoursStr => {
                const formattedHours = this.formatOpeningHours(hoursStr);
                return this.getOpenMinutes(formattedHours);
            })
            .flat();
        return { openingHours, name: restaurant.name };
    })
    .sort((a, b) => a.name < b.name ? - 1 : Number(a.name > b.name));
}

module.exports = Restaurants;
